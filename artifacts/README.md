# Remote Asset API Artifact Cache

This ansible playbook allows for the easy provisioning of a Remote Asset API based Artifact Cache (also Source Cache) compatible with BuildStream master and 1.5.1.

This ansible should be idempotent, that is to say "running this ansible again with no changes should cause no changes on the host".

The host is assumed to be an Ubuntu 20.04 server fresh from Digital Ocean. The user is assumed to have root access to the host through SSH.

## Structure

We have an extra level of inheritance over what is strictly required, as the top level `main.yml` should be extensible to provision multiple servers. `cache.yml` is used to combine the smaller subfiles containing lists of tasks into a single playbook which spins up a cache server from scratch. The remaining files contain lists of tasks relevant to some process in deploying a server.

## The Artifact Cache

The Artifact Cache is a simple docker-compose deployment comprising of a [bb-storage](https://github.com/buildbarn/bb-storage) ContentAddressableStorage (and unused ActionCache), a [bb-remote-asset](https://github.com/buildbarn/bb-remote-asset) Remote Asset API server and a [traefik](https://docs.traefik.io) router.

### Traefik Router

Traffic enters through Traefik, and based upon the port and the gRPC request is routed to the relevant service. Traefik listens on two ports, one for pull, one for push. The only difference in the routing is that Traefik matches specifically to "Pull" gRPC requests in the former, but allows any gRPC request prefixed correctly through in the latter.

Traefik also handles TLS termination. Server certificate generation and renewal from Let's Encrypt is handled entirely by Traefik, which implements an ACME client and keeps certificates up to date. This is specified through CLI arguments in the docker-compose file. Traefik also handles Client TLS Authorisation, which is required for the "Push" endpoint only. The client certificate is held in the `ssl/` directory, and is encrypted using ansible-vault.

### bb-storage

The bb-storage instance holds the data for the artifacts/sources pushed to the cache. This is configured to have an ActionCache, but it is not actually used.

### bb-remote-asset

This implements the Remote Asset API, storing the references for the artifacts/sources pushed. Think of it as an Index to the CAS.
