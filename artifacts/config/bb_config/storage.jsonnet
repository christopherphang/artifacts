{
  blobstore: {
    contentAddressableStorage: {
      'local': {
        keyLocationMapOnBlockDevice: {
          file: {
            path: '/cas/key_location_map',
            sizeBytes: 1024 * 1024 * 1024,
          }
        },
        keyLocationMapMaximumGetAttempts: 8,
        keyLocationMapMaximumPutAttempts: 32,
        oldBlocks: 8,
        currentBlocks: 24,
        newBlocks: 3,
        blocksOnBlockDevice: {
          source: {
            file: {
              path: '/cas/blocks',
              sizeBytes: 200 * 1024 * 1024 * 1024,
            },
          },
          spareBlocks: 3,
        },
        persistent: {
          stateDirectoryPath: '/cas/persistent_state',
          minimumEpochInterval: '300s',
        },
      },
    },
    actionCache: {
      'local': {
        keyLocationMapOnBlockDevice: {
          file: {
            path: '/ac/key_location_map',
            sizeBytes: 1024 * 1024,
          }
        },
        keyLocationMapMaximumGetAttempts: 8,
        keyLocationMapMaximumPutAttempts: 32,
        oldBlocks: 8,
        currentBlocks: 24,
        newBlocks: 3,
        blocksOnBlockDevice: {
          source: {
            file: {
              path: '/ac/blocks',
              sizeBytes: 20 * 1024 * 1024,
            },
          },
          spareBlocks: 3,
        },
        persistent: {
          stateDirectoryPath: '/ac/persistent_state',
          minimumEpochInterval: '300s',
        },
      },
    },
  },
  global: {
    diagnosticsHttpServer: {
      listenAddress: ':9980',
      enablePrometheus: true,
      enablePprof: true,
    },
  },
  grpcServers: [{
    listenAddresses: [':7982'],
    authenticationPolicy: { allow: {} },
  }],
  allowAcUpdatesForInstanceNamePrefixes: [''],
  maximumMessageSizeBytes: 16 * 1024 * 1024,
}
