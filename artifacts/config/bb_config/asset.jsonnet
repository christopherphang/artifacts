{
  // Simple Caching Fetcher, only returns Push'd content
  fetcher: {
    caching: {
      fetcher: {
        'error': {
          code: 5,
          message: "Asset Not Found",
        }
      }
    }
  },

  // On disk storage for references
  assetCache: {
    blobAccess: {
      assetStore: {
        'local': {
          keyLocationMapOnBlockDevice: {
            file: {
              path: '/storage/key_location_map',
              sizeBytes: 1024 * 1024,
            }
          },
          keyLocationMapMaximumGetAttempts: 8,
          keyLocationMapMaximumPutAttempts: 32,
          oldBlocks: 8,
          currentBlocks: 24,
          newBlocks: 3,
          blocksOnBlockDevice: {
            source: {
              file: {
                path: '/storage/blocks',
                sizeBytes: 200 * 1024 * 1024,
              },
            },
            spareBlocks: 3,
          },
          persistent: {
            stateDirectoryPath: '/storage/persistent_state',
            minimumEpochInterval: '300s',
          },
        },
      },
      contentAddressableStorage: {
        grpc: {
          address: "bb-storage:7982",
        }
      },
    },
  },
  global: {
    diagnosticsHttpListenAddress: ':9980',
  },
  grpcServers: [{
    listenAddresses: [':7981'],
    authenticationPolicy: { allow: {} },
  }],
  allowUpdatesForInstances: [''],
  maximumMessageSizeBytes: 16 * 1024 * 1024,
}

